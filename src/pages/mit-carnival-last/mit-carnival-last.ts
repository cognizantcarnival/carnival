import { Component } from '@angular/core';
import { NavController, ToastController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-mit-carnival-last',
  templateUrl: 'mit-carnival-last.html'
})
export class MitCarnivalLast {
  public pageTitle; 
  public themeColor;
  constructor(public navCtrl: NavController,
              public navParams: NavParams) {
      this.pageTitle = navParams.get('title'); 
      this.themeColor = navParams.get('color'); 
}
}

