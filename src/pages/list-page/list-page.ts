import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { DetailPage } from '../detail-page/detail-page';
import { Programma } from '../../providers/programma';
import { Connectivity } from '../../providers/connectivity';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-list-page',
  templateUrl: 'list-page.html',
  providers: [Programma, Connectivity]
})

export class ListPage {
  public pageTitle;
  public themeColor;
  public isProgramma: boolean;
  public noError = false;
  public programs;
  public loading;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public programma: Programma,
    public connectivity: Connectivity,
    public loadingController: LoadingController) {
    this.pageTitle = navParams.get('title');
    this.themeColor = navParams.get('color');
    if (this.pageTitle == "Programma") {
      console.log("Setting programma theme color");
      this.isProgramma = true;
    } else if (this.pageTitle == "Noets") {
      console.log("Setting news themem color")
      this.isProgramma = false;
    }
  }

  ngOnInit() {
    this.loading = this.loadingController.create({
      content: 'Gegevens laaje....'
    });
    this.loading.present();

    if (this.connectivity.isOnline) {
      this.programma.programs(this.pageTitle)
        .map(data => data.json())
        .subscribe(dispData => {
          if (this.pageTitle == "Programma") {
            this.programs = dispData._embedded.tPrograms;
          } else if (this.pageTitle == 'Noets') {
            this.programs = dispData._embedded.tNews;
          }
          console.log("Getting data from " + this.pageTitle);
          console.log(this.programs)
        },
        err => {
          this.noError = true;
          console.log(err);
        });
    }else{
      this.noError = true;
    }
  }

  ngAfterContentInit() {
    this.loading.dismiss();
  }

  itemSelected(item) {
    console.log(item.programdate);
    item.pageTitle = this.pageTitle;
    item.themeColor = this.themeColor;
    item.fabInd = false;
    item.color = this.themeColor;
    if (this.pageTitle == 'Programma') {
      this.navCtrl.push(DetailPage, item);
    }

    if (this.pageTitle == "Noets") {
      item.fabInd = true;
      this.navCtrl.push(DetailPage, item);
    }
  }
}
