import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { QuizQuestions } from '../quiz-questions/quiz-questions';
import { QuizResult } from '../quiz-result/quiz-result';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-quiz-home',
  templateUrl: 'quiz-home.html'
})
export class QuizHome {
  public pageTitle;
  public themeColor;
  public params = { 'title': 'default', 'color': 'default' };
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage) {
    this.pageTitle = navParams.get('title');
    this.themeColor = navParams.get('color');
  }
  gotoQuestions() {
    this.storage.get('zjappquiz').then((result) => {
      if (result == 'passed') {
        this.params.title = this.pageTitle;
        this.params.color = this.themeColor;
        this.navCtrl.push(QuizResult, this.params);
      }else{
        this.storage.set('zjappquiz','failed');
        this.params.title = this.pageTitle;
        this.params.color = this.themeColor;
        this.navCtrl.push(QuizQuestions, this.params);
      }
    }, (err) => {
      return false;
    })
  }
}
