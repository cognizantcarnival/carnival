import { Component } from '@angular/core';
import { NavController, App, ToastController} from 'ionic-angular';
//import all the pages below
import {ListPage} from '../list-page/list-page';
import {GridList} from '../grid-list/grid-list';
import {MitHome} from '../mit-home/mit-home';
import {QuizHome} from '../quiz-home/quiz-home'
import {Proklemasie} from '../proklemasie/proklemasie'
import {DetailPage } from '../detail-page/detail-page';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public params = {
    title: "default",
    color: "default"
  };
  constructor(public navCtrl: NavController,
              public app: App,
              public toastCtrl: ToastController) {
  }

  gotoListPage(pageHeading, clr){
    this.params.title = pageHeading;
    this.params.color = clr;
    this.navCtrl.push(ListPage, this.params);
  }

  gotoGridPage(pageHeading,clr){
    this.params.title = pageHeading;
    this.params.color = clr;
    this.navCtrl.push(GridList, this.params)
  }

  gotoMitHome(pageHeading, clr){
    this.params.title = pageHeading;
    this.params.color = clr;
    this.navCtrl.push(MitHome, this.params)
  }

  gotoQuizHome(pageHeading, clr){
    this.params.title = pageHeading;
    this.params.color = clr;
    this.navCtrl.push(QuizHome, this.params)
  }

  gotoProklemasie(pageHeading, clr){
    this.params.title = pageHeading;
    this.params.color = clr;
    this.navCtrl.push(Proklemasie, this.params)
  }

  displayToast(){
    let toast = this.toastCtrl.create({
      message: "Keumtj d'r aan",
      duration: 3000
    });
    toast.present();
  }
}
