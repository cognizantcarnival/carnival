import { Component } from '@angular/core';
import { NavController, ToastController, NavParams } from 'ionic-angular';
import {MitCarnivalLast} from '../mit-carnival-last/mit-carnival-last';
import {MitHome} from '../mit-home/mit-home';
import {Mitdoon} from '../../providers/mitdoon';
import {Observable } from 'rxjs/Observable';
import {Register} from '../mit-carnival/register';
import {Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'page-mit-carnival',
  templateUrl: 'mit-carnival.html',
  providers: [Mitdoon]
})
export class MitCarnival {
  public pageTitle;
  public themeColor;
  public isProgramma: boolean;
  public noError = false;
  public name:string;
  public phone:string;
  public email:string;
  public role:string;
  public postdata:string;
  public carnivalForm: any;
  public regex: string;
  public code = 'C';
  public params = {
    title: "default",
    color: "default"
  };
  constructor(public navCtrl: NavController,
    public form: FormBuilder,
    public navParams: NavParams,
    public mitdoonvar: Mitdoon,
    public toastCtrl: ToastController) {
    this.pageTitle = navParams.get('title');
    this.themeColor = navParams.get('color');
    this.regex = "/^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i";
    if (this.pageTitle == "CARNIVAL") {
      console.log("Setting CARNIVAL theme color");
    }
    this.carnivalForm = this.form.group({
      "name" : ["", Validators.required],
      "phone":["", Validators.compose([Validators.required, , Validators.minLength(10), Validators.maxLength(10)])],
      "role":["", Validators.required],
      "email":["",Validators.required]
    })
  }

  ionViewDidLoad() {
    console.log('On load MitCarnival Page');
  }

  log(err){
    console.log(err);
  }
  goTOMitDoon(pageHeading, clr) {
    this.params.title = pageHeading;
    this.params.color = clr;
    this.mitdoonvar.mitdoon(this.name,this.phone,this.email,this.role, this.code)
    .subscribe(
      (data) => { 
        if(data.status == 201){
          this.navCtrl.push(MitCarnivalLast, this.params);
        }else{
          alert("Failed:" + data.status)
        }
      },
      (error) => {
        alert(JSON.stringify(error));
      });
  }
}
