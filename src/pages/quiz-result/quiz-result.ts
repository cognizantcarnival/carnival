import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-quiz-result',
  templateUrl: 'quiz-result.html'
})
export class QuizResult {
  public pageTitle;
  public themeColor;
  public voucher;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public storage: Storage) {
    this.pageTitle = this.navParams.get('title');
    this.themeColor = this.navParams.get('color');
  }

  backButtonClick(){
    console.log("Backbutton clicked");
  }

  redeemVoucher() {
    this.storage.get('VoucherStatus').then((status) => {
      console.log(status);
      if(status == 'redeemed'){
        this.showAlert();
      }else{
        this.showPrompt();
      }
    }, (err) => {
      console.log("Error reading the storage");
    })
  }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Voucher Status',
      subTitle: 'dich hebst dien kedo al gatj',
      buttons: ['OK']
    });
    alert.present();
  }

  showError() {
    let alert = this.alertCtrl.create({
      title: 'fout',
      subTitle: 'de ingeveurdje code kloptj neet',
      buttons: ['OK']
    });
    alert.present();
  }

  goHome(){
    this.navCtrl.setRoot(HomePage);
  }

  showSuccess() {
    let alert = this.alertCtrl.create({
      title: 'Inlevere',
      subTitle: 'dien code is herstikke good',
      buttons: ['OK']
    });
    alert.present();
  }

  showPrompt() {
    let prompt = this.alertCtrl.create({
      title: 'Inlevere',
      message: "veur dien juuste code in",
      inputs: [
        {
          name: 'Code',
          placeholder: 'Code'
        },
      ],
      buttons: [
        {
          text: 'stoppe',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Ok',
          handler: data => {
            console.log(data.Code);
            if (data.Code == 'ZMJAPP2017VC') {
              this.storage.set('VoucherStatus', 'redeemed');
              this.showSuccess();
            } else {
              this.showError();
            }
            console.log('Saved clicked');
          }
        }
      ]
    });
    prompt.present();
  }
}
