import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Programma } from '../../providers/programma';
import { Observable } from 'rxjs/Observable';
import { Connectivity } from '../../providers/connectivity';

@Component({
  selector: 'page-proklemasie',
  templateUrl: 'proklemasie.html',
  providers: [Programma, Connectivity]
})
export class Proklemasie {
  public pageTitle;
  public themeColor;
  public noError = false;
  public programs;
  public loading;
  public proklemasie;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public programma: Programma,
    public connectivity: Connectivity,
    public loadingController: LoadingController) {
    this.pageTitle = navParams.get('title');
    this.themeColor = navParams.get('color');
  }

  ngOnInit() {
    this.loading = this.loadingController.create({
      content: 'Gegevens laaje....'
    });
    this.loading.present();
    if (this.connectivity.isOnline) {
      console.log(this.pageTitle);
      this.programma.programs(this.pageTitle)
        .map(data => data.json())
        .subscribe(dispData => {
          this.programs = dispData._embedded.tProklemasies;
          this.proklemasie = this.programs[0].proklemasie;
        },
        err => {
          this.noError = true;
          console.log(err);
        });
    } else {
      this.noError = true;
    }
  }
  ngAfterContentInit() {
    this.loading.dismiss();
  }
}
