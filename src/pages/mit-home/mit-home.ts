import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
//import all the pages below
import {MitCarnival} from '../mit-carnival/mit-carnival';
import {MitBoard} from '../mit-board/mit-board';
import {MitAfternoon} from '../mit-afternoon/mit-afternoon';

@Component({
  selector: 'page-mit-home',
  templateUrl: 'mit-home.html'
})
export class MitHome {
  public pageTitle;
  public themeColor;
  public noError = false;
  public params = {
    title: "default",
    color: "default"
  };


  constructor(public navCtrl: NavController,
              public navParams: NavParams) {
      this.pageTitle = navParams.get('title');
      this.themeColor = navParams.get('color');
  }

  gotoCanaval(pageHeading, clr){
    this.params.title = pageHeading;
    this.params.color = clr;
    console.log("Carnival");
    this.navCtrl.push(MitCarnival, this.params);
  }

  gotoboardof11(pageHeading, clr){
    this.params.title = pageHeading;
    this.params.color = clr;
    console.log("Board of 11");
    this.navCtrl.push(MitBoard, this.params);
  }

  gotoafternoon(pageHeading, clr){
    this.params.title = pageHeading;
    this.params.color = clr;
    console.log("Afternoon");
    this.navCtrl.push(MitAfternoon, this.params);
  }
}
