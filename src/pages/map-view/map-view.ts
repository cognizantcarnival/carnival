import { Component } from '@angular/core';
import { ViewController, NavParams} from 'ionic-angular';
import { Map } from '../../components/map/map';

@Component({
  selector: 'page-map-view',
  templateUrl: 'map-view.html'
})
export class MapView {
  public data;
  public themeColor;
  constructor(public view: ViewController,
              public navParams: NavParams) {
                this.themeColor = this.navParams.get('color');
                this.data = this.navParams.data;
              }
  dismiss(){
    this.view.dismiss();
  }

}
