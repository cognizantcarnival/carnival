import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { MapView } from '../map-view/map-view';
import { SocialSharing } from 'ionic-native';
import { Calendar } from 'ionic-native';

@Component({
  selector: 'page-detail-page',
  templateUrl: 'detail-page.html'
})

export class DetailPage {
  public themeColor;
  public pageTitle;
  public program;
  public hideFab: boolean;
  public loading;
  public modal: any = null;
  constructor(public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams,
    public modalCtrl: ModalController) {
    this.themeColor = navParams.get('color')
    this.pageTitle = navParams.get('pageTitle');
    this.program = navParams.data;
    this.hideFab = this.program.fabInd;
    this.loading = this.loadingCtrl.create({
      content: 'Gegevens laaje....'
    });
  }
  ngOnInit() {
    this.loading.present();
  }
  ngAfterContentInit() {
    this.loading.dismiss();
  }

  displayModal() {
    this.modal = this.modalCtrl.create(MapView, this.program);
    this.modal.present();
  }

  otherShare() {
    let share_message = this.program.programName + "\n\n" +
      this.getFormattedDate(this.program.programdate) + "\n\n" +
      this.program.description;
    let share_subject = this.program.programName;
    SocialSharing.share(share_message, share_subject, null/*File*/, null)
      .then(
      () => {
        console.log("Social Sharing successful");
      },
      () => {
        alert("Failed to share...")
      })
  }

  calender() {
    let dte;
    let calender_title: string;
    if (!this.hideFab) {
      dte = this.program.programdate;
      calender_title = this.program.programName;
    }

    let year = this.getYear(dte);
    let month = this.getMonth(dte) - 1;
    let day = this.getDate(dte);
    let hour = this.getHour(dte);
    //giving a default 1 hour duration
    let endHour = hour + 1;
    let minute = this.getMinutes(dte);

    var startDate = new Date(Date.UTC(year, month, day, hour, minute, 0, 0));

    console.log("Date passed to the calendar")
    console.log(dte);
    console.log(startDate);
    var endDate = new Date(Date.UTC(year, month, day, hour + 1, minute, 0, 0));
    var notes = this.program.description;
    var location = this.program.address;
    Calendar.createEventInteractively(calender_title, location, notes, startDate, endDate)
      .then
      (
      (msg) => { console.log(msg); },
      (err) => { alert(err); console.log(err); }
      );
  }

  getFormattedDate(value) {
    let start = 0;
    let end = value.indexOf("T");
    let event_date = value.slice(start, end) + " ";
    let end2 = value.indexOf(".");
    let event_time = value.slice(end + 1, end2);
    return event_date + event_time;
  }
  getYear(value) {
    //value format 2017-02-11T00:00:00.000+0000  
    let start = 0;
    let end = value.indexOf("-");
    return parseInt(value.slice(start, end));
  }

  getMonth(value) {
    //value format 2017-02-11T00:00:00.000+0000  
    let start = value.indexOf("-") + 1;
    let end = start + 2;
    return parseInt(value.slice(start, end));
  }

  getDate(value) {
    //value format 2017-02-11T00:00:00.000+0000
    let start = value.lastIndexOf("-") + 1;
    let end = start + 2;
    return parseInt(value.slice(start, end));
  }
  getHour(value) {
    //value format 2017-02-11T00:00:00.000+0000
    let start = value.indexOf("T") + 1;
    let end = start + 2;
    return parseInt(value.slice(start, end));
  }
  getMinutes(value) {
    //value format 2017-02-11T00:00:00.000+0000
    var start = value.indexOf(":") + 1;
    var end = start + 2;
    return parseInt(value.slice(start, end));
  }
}
