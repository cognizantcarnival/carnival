import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/*
  Generated class for the MitBoardLast page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-mit-board-last',
  templateUrl: 'mit-board-last.html'
})
export class MitBoardLast {
public pageTitle; 
  public themeColor;
  constructor(public navCtrl: NavController,
              public navParams: NavParams) {
      this.pageTitle = navParams.get('title'); 
      this.themeColor = navParams.get('color'); 
}
}
