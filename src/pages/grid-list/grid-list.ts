import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Programma } from '../../providers/programma';
import { Observable } from 'rxjs/Observable';
import { GridDetail } from '../grid-detail/grid-detail';
import { Connectivity } from '../../providers/connectivity';

@Component({
  selector: 'page-grid-list',
  templateUrl: 'grid-list.html',
  providers: [Programma, Connectivity]
})
export class GridList {
  public pageTitle;
  public themeColor;
  public participants;
  public noError: boolean;
  public error;
  public loading;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public loadingController: LoadingController,
    public connectivity: Connectivity,
    public programma: Programma) {
    this.pageTitle = navParams.get('title');
    this.themeColor = navParams.get('color');
  }

  ngOnInit() {
    this.loading = this.loadingController.create({
      content: "Gegevens laaje..."
    })
    this.loading.present();

    if (this.connectivity.isOnline) {
      this.programma.programs(this.pageTitle)
        .map(data => data.json())
        .subscribe(dispData => {
          this.noError = false;
          this.participants = dispData._embedded.tParticipants;
        },
        err => {
          this.noError = true;
          console.log(err);
        });
    }else{
      this.noError = true;
    }
  }

  ngAfterContentInit() {
    this.loading.dismiss();
  }

  showDetail(details) {
    details.pageTitle = this.pageTitle;
    details.themeColor = this.themeColor;
    this.navCtrl.push(GridDetail, details);
  }
}
