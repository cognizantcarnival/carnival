import { Component } from '@angular/core';
import { NavController, ToastController, NavParams } from 'ionic-angular';
import {MitHome} from '../mit-home/mit-home';
import {Mitdoon} from '../../providers/mitdoon';
import { Observable } from 'rxjs/Observable';
import { MitBoardLast } from '../mit-board-last/mit-board-last';
import {Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'page-mit-board',
  templateUrl: 'mit-board.html',
  providers: [Mitdoon]
})
export class MitBoard {
  public pageTitle;
  public themeColor;
  public noError = false;
   public boardForm: any;
  public name:string;
  public phone:string;
  public email:string;
  public role = 'n/a';
  public code = "B"
  public params = {
    title: "default",
    color: "default"
  };
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public form: FormBuilder,
    public mitdoonvar: Mitdoon,
    public toastCtrl: ToastController) {
    this.pageTitle = navParams.get('title');
    this.themeColor = navParams.get('color');
    if (this.pageTitle == "BOARD OF 11") {
      console.log("Setting BOARD OF 11 theme color");
    }
    this.boardForm = this.form.group({
      "name" : ["", Validators.required],
      "phone":["", Validators.compose([Validators.required, , Validators.minLength(10), Validators.maxLength(10)])],
      "email":["",Validators.required]
    })
  }
  ionViewDidLoad() {
    console.log('On load MitBoard Page');
  }

  goTOMitDoon(pageHeading, clr) {
    this.params.title = this.pageTitle;
    this.params.color = this.themeColor;
    this.mitdoonvar.mitdoon(this.name,this.phone,this.email,this.role, this.code).subscribe(
      (data) => {
        if(data.status == 201){
          this.navCtrl.push(MitBoardLast, this.params);
        }else{
          alert("Failed:" + data.status)
        }
      },
      (error) => {
        alert(JSON.stringify(error));
      }
    );
  }

}
