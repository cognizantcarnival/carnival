import { Component } from '@angular/core';
import { NavController, ToastController, NavParams } from 'ionic-angular';
import {Mitdoon} from '../../providers/mitdoon';
import { Observable } from 'rxjs/Observable';
import { MitAfternoonLast } from '../mit-afternoon-last/mit-afternoon-last';
import {Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'page-mit-afternoon',
  templateUrl: 'mit-afternoon.html',
  providers: [Mitdoon]
})
export class MitAfternoon {
  public pageTitle;
  public themeColor;
  public isProgramma: boolean;
  public noError = false;

  public name: string;
  public phone: string;
  public email: string;
  public afternoonForm: any;
  public role: string;
  public code = "V";

  public params = {
    title: "default",
    color: "default"
  };
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public mitdoonvar: Mitdoon,
    public form: FormBuilder,
    public toastCtrl: ToastController) {
    this.pageTitle = navParams.get('title');
    this.themeColor = navParams.get('color');
    if (this.pageTitle == "VARIATE AFTERNOON") {
      console.log("Setting VARIATE AFTERNOON theme color");
    }
    this.afternoonForm = this.form.group({
      "name" : ["", Validators.required],
      "phone":["", Validators.compose([Validators.required, , Validators.minLength(10), Validators.maxLength(10)])],
      "role":["", Validators.required],
      "email":["",Validators.required]
    })
  }

  ionViewDidLoad() {
    console.log('Hello MitAfternoon Page');
  }

  goTOMitDoon(pageHeading, clr) {
    this.params.title = this.pageTitle;
    this.params.color = this.themeColor;
    console.log('Call Mitdoon Provider');
    this.mitdoonvar.mitdoon(this.name, this.phone, this.email, this.role, this.code).subscribe(
      (data) => {
         if(data.status == 201){
          this.navCtrl.push(MitAfternoonLast, this.params);
        }else{
          alert("Failed:" + data.status)
        }        
      },
      (error) => {
        alert(JSON.stringify(error));
      }
    );
  }
}
