import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Slides, AlertController } from 'ionic-angular';
import { Programma } from '../../providers/programma';
import { ViewChild } from '@angular/core';
import { QuizResult } from '../quiz-result/quiz-result';
import { Storage } from '@ionic/storage';
import { Connectivity } from '../../providers/connectivity';

@Component({
  selector: 'page-quiz-questions',
  templateUrl: 'quiz-questions.html',
  providers: [Programma, Connectivity]

})

export class QuizQuestions {
  public loading;
  public pageTitle;
  public themeColor;
  public error;
  public params = { 'title': 'default', 'color': 'default', 'result': 'failed' };
  public noError = false;
  public quizData;
  public success_text = false;
  public answersMap = {};
  public answerMapSize = 0;
  public disableSubmit = true;
  public selectedAnswer = false;
  @ViewChild('mySlider') slider: Slides;
  mySlideOptions = {
    pager: false
  };

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public programma: Programma,
    public connectivity: Connectivity,
    public alertCtrl: AlertController,
    public loadingController: LoadingController,
    public storage: Storage) {
    this.pageTitle = navParams.get('title');
    this.themeColor = navParams.get('color');
  }

  ngOnInit() {
    this.loading = this.loadingController.create({
      content: 'Gegevens laaje....'
    });
    this.loading.present();
    if (this.connectivity.isOnline) {
    console.log(this.pageTitle);
    this.programma.programs(this.pageTitle)
      .map(data => data.json())
      .subscribe(dispData => {
        this.quizData = dispData._embedded.tQuizzes;
        console.log(this.quizData)
      },
      err => {
        this.noError = true;
        console.log(err);
      });
    }else{
      this.noError = true;
    }
  }

  ngAfterContentInit() {
    this.loading.dismiss();
  }

  onSubmit() {
    var allCorrect = (this.answerMapSize === this.quizData.length);
    var answerIndex = 0;
    while (allCorrect && answerIndex < this.answerMapSize) {
      allCorrect = !!this.answersMap[answerIndex];
      answerIndex++;
    }
    if (allCorrect) {
      //store the quiz results
      this.storage.set('zjappquiz', 'passed');
      //navigate to results page
      this.params.title = this.pageTitle;
      this.params.color = this.themeColor;
      this.params.result = "passed";
      this.navCtrl.push(QuizResult, this.params);
    } else {
      this.showAlert();
    }
  }

  ionAnswerSelect(questionIndex, answerId) {
    var currentQuestion = this.quizData[questionIndex];
    this.answersMap[questionIndex] = (currentQuestion.correctanswer == answerId);
    this.answerMapSize = Object.keys(this.answersMap).length;
    console.log(this.answersMap, currentQuestion.correctanswer, answerId);
    console.log(this.slider.isEnd());
    this.selectedAnswer = true;
    if (this.slider.isEnd()) {
      this.disableSubmit = false;
    }
    if (!this.slider.isEnd()) {
      this.slider.slideNext(1000);
    }
  }
  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Joamer, dich hebst neet alles good',
      subTitle: "probeer 't nog es",
      buttons: ['OK']
    });
    alert.present();
  }
}