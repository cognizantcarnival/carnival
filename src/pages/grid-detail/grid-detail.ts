import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';

/*
  Generated class for the GridDetail page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-grid-detail',
  templateUrl: 'grid-detail.html'
})
export class GridDetail {
  public pageTitle;
  public participantInfo;
  public loading;
  public themeColor;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public loadingController: LoadingController) {
     console.log("Data recieved at grid detail page");
     console.log(navParams);
    
     this.participantInfo = navParams.data;
      this.pageTitle = this.participantInfo.pageTitle;
     this.themeColor = this.participantInfo.themeColor;
     console.log(this.themeColor);
  }
ngOnInit(){
    this.loading = this.loadingController.create({
      content: "Gegevens laaje..."
    })
    this.loading.present();
  }

ngAfterContentInit() {
    this.loading.dismiss();
  }
}
