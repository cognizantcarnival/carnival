import { Component, Input } from '@angular/core';
import { LoadingController, App } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
declare var google: any;

@Component({
  selector: 'map',
  templateUrl: 'map.html'
})
export class Map {
  @Input() public address: string;
  public map;
  public location;
  public loading;
  constructor(public loadingController: LoadingController, public app: App) {
 //   this.app.setScrollDisabled(true);
  }

  ngOnInit() {
    this.loading = this.loadingController.create({
      content: "Loading Map..."
    });
    this.loading.present();
    this.map = this.createMap();
    this.programLocation(this.loading, this.address, this.map);
  }

  createMap() {
    var dflt_latlng = new google.maps.LatLng(52.132633, 5.291266);
    let mapOptions = {
      center: dflt_latlng,
      fullscreenControl: true,
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true
    }

    let mapElement = document.getElementById("map");
    let map = new google.maps.Map(mapElement, mapOptions);
    return map;
  }

  programLocation(load, loc, resultsMap) {
    var infowindow = new google.maps.InfoWindow({
      content: loc
    });
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': this.address }, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        resultsMap.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
          map: resultsMap,
          position: results[0].geometry.location
        });
        marker.addListener('click', function() {
          infowindow.open(resultsMap, marker);
        });
        infowindow.open(resultsMap, marker);
        load.dismiss();
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
  }
}
