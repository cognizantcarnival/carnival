import { Component, Input } from '@angular/core';

@Component({
  selector: 'error',
  templateUrl: 'error.html'
})
export class Error {
  @Input() public themeColor: string;
  constructor() {
    console.log('Hello Error Component');
    console.log(this.themeColor);
  }

}
