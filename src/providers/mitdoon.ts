import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions, RequestMethod, Request } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import { Register } from '../mit-carnival/register';

@Injectable()
export class Mitdoon {
  constructor(public http: Http) {
    console.log('Hello Mitdoon Provider');
  }

  mitdoon(name, phone, email, role, code) {
    let url = 'http://ec2-52-59-221-107.eu-central-1.compute.amazonaws.com:8091/tUsers';
    //let url = 'http://192.168.0.108:8091/tUsers';
    let body1 = {
      email: email,
      mitCode: code,
      mobile: phone,
      name: name,
      wtWillJeDoen: role
    };

    var headers = new Headers();

    headers.append('Content-Type', 'application/hal+json;charset=utf-8');

    let options = new RequestOptions({
      headers: headers,
      url: url,
      method: RequestMethod.Post,
      body: body1
    });

    return this.http.request(new Request(options))
      .map((res: Response) => {
        if (res) {
          return { status: res.status, json: res.json() }
        }
      });
  }
}
