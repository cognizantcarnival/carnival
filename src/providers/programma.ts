import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class Programma {

  constructor(public http: Http) {
  }
  programs(title){
    let url = 'http://ec2-52-59-221-107.eu-central-1.compute.amazonaws.com:8091/';
    if(title == "Programma"){
      url = url + "tPrograms/search/upcoming";
    }
    if(title == "Noets"){
      url = url + 'tNews/search/current'
    }
    if (title == "Wae stèlle veur"){
      url = url + 'tParticipants'
    }

    if (title == "Proklemasie"){
      url = url + 'tProklemasies'
    }

    if (title == "Vastelaoves Kwis"){
      url = url + 'tQuizzes'
    }

    console.log(url);
    return this.http.get(url);
  }
}
