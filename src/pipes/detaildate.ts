import { Injectable, Pipe } from '@angular/core';

@Pipe({
  name: 'detaildate'
})
@Injectable()
export class Detaildate {

  transform(value) {
    //value format 2017-02-11T00:00:00.000+0000   
    var detaildate;
    var months = ['JANUARI','FEBRUARI','MAART','APRIL','MEI','JUNI','JULI','AUGUSTUS','SEPTEMBER','OKTOBER','NOVEMBER','DECEMBER'];
    value = value + ''; // make sure it's a string
    var start = value.lastIndexOf("-") + 1;
    var end = start + 2;
    var dte = parseInt(value.slice(start, end));

    var mStart = value.indexOf("-") + 1;
    var mEnd = value.lastIndexOf("-");
    var month = parseInt(value.slice(mStart, mEnd));
    var tStartH = value.indexOf("T") + 1;
    if(tStartH){
      var tEndH = value.lastIndexOf(":");
      var time = value.slice(tStartH, tEndH);
      detaildate = dte + ' ' + months[month - 1] + ' - ' + time;
    }else{
      detaildate = dte + ' ' + months[month - 1];
    }
    console.log(detaildate);
    return detaildate;
  }
}
