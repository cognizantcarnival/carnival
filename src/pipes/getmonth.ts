import { Injectable, Pipe } from '@angular/core';

@Pipe({
  name: 'getmonth'
})
@Injectable()
export class Getmonth {
  transform(value) {
    //value format 2017-02-11T00:00:00.000+0000
    var months = ['Jan','Feb','Maa','Apr','Mei','Jun','Jul','Aug','Sep','Okt','Nov','Dec'];
    value = value + ''; // make sure it's a string
    var start = value.indexOf("-") + 1;
    var end = value.lastIndexOf("-");
    var month = parseInt(value.slice(start, end));
    return months[month - 1];
  }
}
