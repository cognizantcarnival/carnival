import { Injectable, Pipe } from '@angular/core';

@Pipe({
  name: 'getdate'
})
@Injectable()
export class Getdate {

  transform(value) {
    //value format 2017-02-11T00:00:00.000+0000
    value = value + ''; // make sure it's a string
    //extract the date from value
    var start = value.lastIndexOf("-") + 1;
    var end = start + 2;
    var dte = parseInt(value.slice(start, end));
    return dte;
  }
}
