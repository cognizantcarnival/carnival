import { Component, ViewChild } from '@angular/core';
import { Platform, IonicApp, Nav } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { HomePage } from '../pages/home/home';
import { QuizResult } from '../pages/quiz-result/quiz-result';


@Component({
  template: `<ion-nav id="nav" #content [root]="rootPage"></ion-nav>`
})
export class MyApp {
  rootPage = HomePage;
  @ViewChild(Nav) nav: Nav;

  constructor(private platform: Platform, 
              private ionicApp: IonicApp) {
    platform.ready().then(() => {
      StatusBar.styleDefault();
      platform.registerBackButtonAction(() => {
        let activeModal = ionicApp._modalPortal.getActive() || ionicApp._loadingPortal.getActive() ||
                          ionicApp._toastPortal.getActive() || ionicApp._overlayPortal.getActive();
        let activeNav = this.nav;
        let view = this.nav.getActive();
        if(activeModal){
          activeModal.dismiss();
        }else if(view.instance instanceof QuizResult){
          this.nav.setRoot(HomePage)
        }else if(activeNav.canGoBack()){
            activeNav.pop();
        }else{
            this.platform.exitApp();
          }
      });
    });
  }
}
