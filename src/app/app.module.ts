import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list-page/list-page';
import { DetailPage } from '../pages/detail-page/detail-page';
import { MitHome } from '../pages/mit-home/mit-home';
import { GridList } from '../pages/grid-list/grid-list';
import { Proklemasie } from '../pages/proklemasie/proklemasie';
import { MitBoard } from '../pages/mit-board/mit-board';
import { MitCarnival } from '../pages/mit-carnival/mit-carnival';
import { MitAfternoon } from '../pages/mit-afternoon/mit-afternoon';
import { MitCarnivalLast } from '../pages/mit-carnival-last/mit-carnival-last';
import { MitAfternoonLast } from '../pages/mit-afternoon-last/mit-afternoon-last';
import { MitBoardLast } from '../pages/mit-board-last/mit-board-last';
import { QuizHome } from '../pages/quiz-home/quiz-home';
import { QuizQuestions } from '../pages/quiz-questions/quiz-questions';
import { QuizResult } from '../pages/quiz-result/quiz-result';
import { GridDetail } from '../pages/grid-detail/grid-detail';
import { MapView } from '../pages/map-view/map-view';
import { Map } from '../components/map/map';
import { Error } from '../components/error/error';
import { Getmonth } from '../pipes/getmonth';
import { Getdate } from '../pipes/getdate';
import { Detaildate } from '../pipes/detaildate';
import { Storage } from '@ionic/storage';

export function provideStorage() {
  return new Storage();
  // optional config);
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    DetailPage,
    MitHome,
    GridList,
    Proklemasie,
    MitBoard,
    MitCarnival,
    MitAfternoon,
    MitCarnivalLast,
    MitBoardLast,
    MitAfternoonLast,
    QuizHome,
    QuizQuestions,
    QuizResult,
    MapView,
    GridDetail,
    Map,
    Error,
    Getdate,
    Getmonth,
    Detaildate
  ],
  imports: [
    IonicModule.forRoot(MyApp, {
      backButtonIcon : 'ios-arrow-round-back',
      backButtonText: ''
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    DetailPage,
    MitHome,
    GridList,
    Proklemasie,
    MitBoard,
    MitCarnival,
    MitAfternoon,
    MitBoardLast,
    MitAfternoonLast,
    MitCarnivalLast,
    QuizHome,
    QuizQuestions,
    QuizResult,
    GridDetail,
    MapView,
    Map,
    Error
  ],
  providers: [{ provide: Storage, useFactory: provideStorage }]
})
export class AppModule {}


